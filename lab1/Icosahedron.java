package lab1;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Icosahedron {
    public static void main(String[] args) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(System.in);
            System.out.print("Длина ребра икосаэдра: ");
            double edge = scanner.nextDouble();
            double volume = ((15 + 5 * Math.sqrt(5)) / 12) * Math.pow(edge, 3);
            System.out.println("Объем = " + volume);
        }
        catch (InputMismatchException e) {
            System.out.println(e.getClass());
        } 
        catch (Exception e) {
            e.printStackTrace();
        } 
    }
}
