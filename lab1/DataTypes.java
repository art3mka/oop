package lab1;
public class DataTypes {
    public static void main(String[] args) {

        //byte 
        //sorted in 8 bits of memory and stores values from (-2^7) to (2^7)-1
        System.out.println("max and min value of byte data type: " + Byte.MAX_VALUE + " and " + Byte.MIN_VALUE);
        //short
        //sorted in 16 bits of memory and stores values from (-2^15) to (2^15)-1
        System.out.println("max and min value of short data type: " + Short.MAX_VALUE + " and " + Short.MIN_VALUE);
        //int
        //sorted in 32 bits of memory and stores values from (-2^31) to (2^31)-1
        System.out.println("max and min value of int data type: " + Integer.MAX_VALUE + " and " + Integer.MIN_VALUE);
        //long
        //sorted in 64 bits of memory and stores values from (-2^63) to (2^63)-1
        System.out.println("max and min value of long data type: " + Long.MAX_VALUE + " and " + Long.MIN_VALUE);
        //float
        //sorted in 32 bits (IEEE 754) of memory
        System.out.println("max and min value of float data type: " + Float.MAX_VALUE + " and " + Float.MIN_VALUE);
        //double
        //sorted in 64 bits (IEEE 754) of memory
        System.out.println("max and min value of double data type: " + Double.MAX_VALUE + " and " + Double.MIN_VALUE); 
        
    }
}
