package lab1;
import java.util.InputMismatchException;
import java.util.Scanner;


public class HelloUser {
    public static void main(String[] args) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(System.in);
            System.out.print("Имя пользователя: ");
            String username = scanner.nextLine();
            System.out.println("Привет, " + username + "!");
        }
        catch (InputMismatchException e) {
            System.out.println(e.getClass());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
