package week2.task3;

import java.util.Scanner;

public class MergeSort {
    public static void main(String args[]){   
        Scanner input = new Scanner(System.in);
        System.out.print("Длина массива: ");
        int arraySize = input.nextInt();
        int dataSet[] = new int[arraySize];
        System.out.println("Введите элементы массива: ");
        
        for (int i = 0; i < arraySize; i++) {
            dataSet[i] = input.nextInt();
        }
 
        int n = dataSet.length;
        System.out.println("Данный массив:");
        for (int i = 0; i < n; ++i)
            System.out.print(dataSet[i] + " ");
        System.out.println();
 
        MergeSort data = new MergeSort();
        data.sort(dataSet, 0, dataSet.length - 1);
 
        System.out.println("Отсортированный массив:");
        for (int i = 0; i < n; ++i)
            System.out.print(dataSet[i] + " ");
        System.out.println();
        input.close();
    }
    public static void merge(int dataSet[], int l, int m, int r) {
        // Find sizes of two subarrays
        int n1 = m - l + 1;
        int n2 = r - m;
 
        // Temp arrays
        int left[] = new int[n1];
        int right[] = new int[n2];
 
        // Copy data to temp arrays
        for (int i = 0; i < n1; ++i)
            left[i] = dataSet[l + i];
        for (int j = 0; j < n2; ++j)
            right[j] = dataSet[m + 1 + j];

        int i = 0; 
        int j = 0;
 
        // index of merged subarray array
        int k = l;
        while (i < n1 && j < n2) {
            if (left[i] <= right[j]) {
                dataSet[k] = left[i];
                i++;
            }
            else {
                dataSet[k] = right[j];
                j++;
            }
            k++;
        }
        while (i < n1) {
            dataSet[k] = left[i];
            i++;
            k++;
        }
        while (j < n2) {
            dataSet[k] = right[j];
            j++;
            k++;
        }
    }
 
    void sort(int dataSet[], int l, int r) {
        if (l < r) {
            // Find the middle point
            int m = l + (r - l) / 2;
 
            // Sort first and second halves
            sort(dataSet, l, m);
            sort(dataSet, m + 1, r);
 
            // Merge the sorted halves
            merge(dataSet, l, m, r);
        }
    }
}
