package week2.task3;

import java.util.Arrays;
import java.util.Scanner;

public class MaxEqual {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Длина массива: ");
        int arraySize = input.nextInt();
        int dataSet[] = new int[arraySize];
        System.out.println("Введите элементы массива: ");
        
        for (int i = 0; i < arraySize; i++) {
            dataSet[i] = input.nextInt();
        }
        System.out.println("Введенные элементы массива: ");
        for (int i = 0; i < arraySize; i++) {
            System.out.print(dataSet[i] + ", ");
        }
        System.out.println();
        Arrays.sort(dataSet);
        int max = dataSet[arraySize - 1];
        int maxCount = 0;
        for (int i = 0; i < arraySize; i++) {
            if (dataSet[i] == max) {
                maxCount++;
            }
        }
        System.out.println("Количество элементов равных максимальному: " + maxCount);
        input.close();
    }
}

