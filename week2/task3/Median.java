package week2.task3;

import java.util.Arrays;
import java.util.Scanner;

public class Median {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Длина массива: ");
        int arraySize = input.nextInt();
        double dataSet[] = new double[arraySize];
        System.out.println("Введите элементы массива:");
    
        for (int i = 0; i < arraySize; i++) {
            dataSet[i] = input.nextInt();
        }
        System.out.println("Введенные элементы массива:");
        for (int i = 0; i < arraySize; i++) {
            System.out.print(dataSet[i] + ", ");
        }
        System.out.println();
        Arrays.sort(dataSet);
        if (dataSet.length % 2 != 0) {
            System.out.println("Медиана: " + dataSet[dataSet.length/2]);
        }
        else {
            System.out.println("Медиана: " + (dataSet[dataSet.length/2] 
            + dataSet[dataSet.length/2-1])/2);
        }
        input.close();    
    }
}