package week2.task2;
import java.util.Scanner;  
public class PrimeNumbers {  
   public static void main(String[] args) {  
       Scanner input = new Scanner(System.in);  
       System.out.print("Введите число, до которого необходимо найти простые числа : ");  
       int end = input.nextInt();  
       System.out.println("Список простых чисел от " + 1 + " до " + end);  
       for (int i = 1; i <= end; i++) {  
           if (isPrime(i)) {  
               System.out.println(i);  
           }  
       } 
    input.close(); 
   }  
   public static boolean isPrime(int n) {  
       if (n <= 1) {  
           return false;  
       }  
       for (int i = 2; i <= Math.sqrt(n); i++) {  
           if (n % i == 0) {  
               return false;  
           }  
       }  
       return true;  
   }  
}  
    