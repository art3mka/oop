package week2.task2;

import java.util.Scanner;

public class Pyramid {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.print("Высота пирамиды: ");
        int height = input.nextInt();
        for (int i = 1; i <= height; i++) {
            for (int j = height; j >= i; j--) {
                System.out.print(" ");
            }
            for (int j = 1; j <= i; j++) {
                System.out.print("#");
            }
            System.out.print("  ");
            for (int j = 1; j <= i; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
        input.close(); 
    }
}
