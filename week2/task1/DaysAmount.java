package week2.task1;

import java.util.Scanner;

public class DaysAmount {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите номер месяца и год: ");
        int month = input.nextInt();
        int year = input.nextInt();
        
        if (month == 4 || month == 6 || month == 9 || month == 11) {
            System.out.print("В месяце 30 дней");
        }
        else if (month == 2) {
            if (year % 4 == 0) {
                System.out.printf("В месяце 29 дней. Т.к. %d год - високосный", year);
            }
            else {
                System.out.printf("В месяце 28 дней. Т.к. %d год - не високосный", year);
            }
        }
        else{
            System.out.print("В месяце 31 день");
        }
        input.close();
    }
}
        
