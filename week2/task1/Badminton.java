package week2.task1;

import java.util.Scanner;

public class Badminton {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Хотите ли вы играть в бадминтон?");
        System.out.println("Сегодня воскресенье? да/нет");
        String day = userInput.nextLine();
        switch (day) {
            case "да":
            System.out.println("Какая сегодня температура? жарко/тепло/"
            + "холодно");
            String weather = userInput.nextLine();
            switch (weather) {
                case "жарко":
                case "холодно":
                System.out.println("Нет");
                break;

                case "тепло":
                System.out.println("Есть ли сегодня ветер? да/нет");
                String wind = userInput.nextLine();
                switch (wind) {
                    case "да":
                    System.out.println("Нет");
                    break;

                    case "нет":
                    System.out.println("Какая сегодня влажность? низкая/высокая");
                    String humidity = userInput.nextLine();
                    switch (humidity) {
                        case "низкая":
                        System.out.println("Нет");
                        break;

                        case "высокая":
                        System.out.println("Какие сегодня осадки? ясно/облачно/" 
                        + "дождь/снег/град");
                        String precipitation = userInput.nextLine();
                        switch (precipitation) {
                            case "дождь":
                            case "снег":
                            case "град":
                            System.out.println("Нет");
                            break;

                            case "ясно":
                            case "облачно":
                            System.out.println("Да");
                            break;
                        }
                        break;
                    }
                    break;
                }
                break;
            }
            break;

            case "нет":
            System.out.println("Нет");
            break;
        }
        userInput.close();
    }
}

