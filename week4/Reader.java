package week4;

import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

public class Reader {
    public static void main(String[] args) throws IOException{
        fileRead("week4/data.txt");
    }

    public static void fileRead(String fileName) throws IOException {
        try {
            FileReader reader = new FileReader(fileName);

            int text;

            while ((text = reader.read()) != -1) {
                System.out.print((char)text);
            }
            reader.close();
            
        } catch (FileNotFoundException e) {
            System.out.print("Required file not found");
        }
    }
}