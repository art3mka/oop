package week4;

import java.util.Scanner;

public class Converter {
    public static void main(String args[]) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Сообщение:"); 
            String text = input.nextLine(); 
            System.out.println("Тип данных\n(boolean, byte," +
            " short, int, integer, float, long, double):"); 
            String typeToConvertTo = input.nextLine();

            try {
                System.out.println(transferer(text, typeToConvertTo));
            } catch (NumberFormatException e) {
                System.out.println("Это сообщение нельзя " +
                "преобразовать в выбранный тип");
            }
        } 
    }
    
    public static Object transferer(String text, String typeToConvertTo) {
        switch (typeToConvertTo) {
            case "boolean":
                return Boolean.parseBoolean(text);
            case "byte":
                return Byte.parseByte(text);
            case "short":
                return Short.parseShort(text);
            case "int":
                return Integer.parseInt(text);
            case "integer":
                return Integer.parseInt(text);
            case "float":
                return Float.parseFloat(text);
            case "long":
                return Long.parseLong(text);
            case "double":
                return Double.parseDouble(text);
            default:
                return "Тип данных введен неверно";
        }
    }
}
