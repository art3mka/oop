package week4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Sensors {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Введите показания: ");
            String data = input.nextLine();

            try {
                if (data.length() > 512) {
                    throw new StringOutOfRangeException();
                }
            } catch (StringOutOfRangeException e) {
                System.out.println(e);
                System.exit(0);
            }

            System.out.println("По какому полю сортировать " 
            + "(1 - по id; 2 - по средней температуре): ");
            int sortBy = input.nextInt();
        
            double[][] dataArray = dataProcessing(data);
            averages(dataArray);

            if (sortBy == 1) {
                Arrays.sort(dataArray, Comparator.comparingDouble(x -> x[0]));
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i][0] != 0) {
                        System.out.printf("%.0f %.1f%n", dataArray[i][0], dataArray[i][1]);
                    } 
                }
            } else {
                Arrays.sort(dataArray, Comparator.comparingDouble(x -> x[1]));
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i][0] != 0) {
                        System.out.printf("%.0f %.1f%n", dataArray[i][0], dataArray[i][1]);
                    }
                } 
            }
        }
    }
        
    public static void averages(double[][] dataArray) {
        for (int i = 0; i < dataArray.length; i++) {
            int count = 1;
            for (int j = 0; j < dataArray.length; j++) {
                if (dataArray[i][0] == dataArray[j][0] && i != j) {
                    dataArray[i][1] += dataArray[j][1];
                    count++;
                    dataArray[j][0] = 0;
                }
            }
            dataArray[i][1] = dataArray[i][1] / count; 
        }
    }

    public static double[][] dataProcessing(String line) {
        String[] data = line.split("@");
        double[][] dataArray = new double[data.length][2];

        for (int i = 0; i < data.length; i++) {
            double id = Double.parseDouble(data[i].substring(0, 2));
            double temperature = Double.parseDouble(data[i].substring(2));
            
            double[] tuple = {id, temperature};
            dataArray[i] = tuple;
        }

        return dataArray;
    }
}
