package task1;

import java.util.Scanner;

public class Matches {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Что ищем: ");
            String subString = input.nextLine();
            System.out.println("Где ищем: ");
            int counter = 0;
            while (true) {
                String text = input.nextLine();
                if (text.isEmpty()) {
                    break;
                } else {
                    counter += substringMatches(subString, text);
                }
            }
            System.out.println(counter);
        }
    }

    public static int substringMatches(String subString, String text) {
        int counter = 0;
        while (text.contains(subString)){
            if (subString.length() < 1) {
                text = text.substring(text.indexOf(subString) + subString.length());
            } 
            else {
                text = text.substring(text.indexOf(subString) + subString.length()-1);
            }
            counter++;
        }
        return counter;
    }
}