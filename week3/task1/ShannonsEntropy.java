package task1;

import java.util.Arrays;
import java.util.Scanner;

public class ShannonsEntropy {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            String text = input.nextLine();
            System.out.printf("%.2f", entropyCalculate(text));
        }  
    }
    
    public static int getSum(double[] list) {
        int sum = 0;
        for (int i = 0; i < list.length; i++) {
            if (list[i] > 0) {
                sum += list[i];
            }
        }
        return sum;
    }

    public static double entropyCalculate(String line) {
        char[] chars = line.toCharArray();
        Arrays.sort(chars);

        double[] countChars = new double[Character.MAX_VALUE];

        for (int i = 0; i < line.length(); i++) {
            char charAt = line.charAt(i);
            countChars[charAt]++;
        }

        Arrays.sort(countChars);
        int countSum = getSum(countChars);

        for (int i = 0; i < countChars.length; i++) {
            if (countChars[i] > 0) {
                countChars[i] /= countSum;
            } 
        }

        double entropy = 0;
        for (int i = 0; i < countChars.length; i++) {
            if (countChars[i] > 0) {
                entropy -= countChars[i] * (Math.log(countChars[i]) / (Math.log(2)));
            } 
        }

        return entropy;
    }
}