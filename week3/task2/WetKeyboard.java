package task2;

import java.util.Scanner;

public class WetKeyboard {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите слово: ");
            String word = scanner.nextLine();
            word = word.replaceAll("(.)\\1{2,}", "$1");
            System.out.println(word);
        }
    }
}
