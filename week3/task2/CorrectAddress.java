package task2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CorrectAddress {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Ваш полный IPv6 адрес: ");
            String address = input.nextLine();
            String pattern = "^(?:[0-9A-f]{4}:){7}[0-9A-f]{1,4}$";
            System.out.println(validation(address, pattern));
        } 
    }

    public static String validation(String text, String ipv6Pattern) {
        Pattern pattern = Pattern.compile(ipv6Pattern);
        Matcher matcher = pattern.matcher(text);
        String output = "";
        if (matcher.matches() == true) {
           output = "Адрес верный";
        } 
        else {
            output = "Адрес неверный";
        } 
        return (output);
    }
}
