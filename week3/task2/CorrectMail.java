package task2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CorrectMail {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Ваш адрес эл.почты: ");
            String address = input.nextLine();
            String pattern = "^[^.]+[A-z0-9+_.-]+@[A-z0-9_\\.-]+.[A-z]$";
            System.out.println(validation(address, pattern));
        }    
    }
    
    public static String validation(String text, String emailPattern) {
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(text);
        String output = "";
        if (matcher.matches() == true) {
           output = "Адрес эл.почты введен верно";
        } 
        else {
            output = "Адрес эл.почты введен неверно";
        } 
        return (output);
    }
}
