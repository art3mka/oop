package week5.task3;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashSet;

public class Duplicates {
    public static void main(String[] args){
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Введите элементы входного массива: ");
            String arrayElements = input.nextLine();
            ArrayList<Integer> elementsList = week5.task1.WaterSquare.stringConverter(arrayElements);
            
            System.out.print("Входной массив: " + elementsList);
            HashSet<Integer> elementsSet = new HashSet<Integer>(elementsList);
            System.out.print("\nРезультат: " + elementsSet);
        }
        catch (NumberFormatException e) {
            System.out.println(e.getClass());
        }
    }
}
