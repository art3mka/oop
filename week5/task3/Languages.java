package week5.task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class Languages {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            ArrayList<ArrayList<String>> userArray = new ArrayList<ArrayList<String>>();
            ArrayList<String> tempArray = new ArrayList<String>();
            System.out.print("Количество школьников: ");
            int studentsAmount = input.nextInt();
            for (int i = 0; i < studentsAmount; i++) {
                System.out.print("Количество языков: ");
                int LanguagesAmount = input.nextInt();
                System.out.println("Введите языки: ");
                for (int k = 0; k < LanguagesAmount; k++) {
                    tempArray.add(input.next());
                }
                userArray.add(new ArrayList<>(tempArray));
                tempArray.clear();
            }
            
            HashSet<String> crossing = findCrossing(userArray);
            HashSet<String> union = findUnion(userArray);

            String[] crossingArray = crossing.toArray(new String[0]);
            Arrays.sort(crossingArray);

            String[] unionArray = union.toArray(new String[0]);
            Arrays.sort(unionArray);

            System.out.println();
            System.out.println(crossingArray.length);
            for (int i = 0; i < crossingArray.length; i++) {
                System.out.println(crossingArray[i]);
            }
            
            System.out.println(unionArray.length);
            for (int i = 0; i < unionArray.length; i++) {
                System.out.println(unionArray[i]);
            }

        }
    }

    static HashSet<String> findCrossing(ArrayList<ArrayList<String>> userArray) {
        HashSet<String> crossing = new HashSet<>(userArray.get(0));

        for (int i = 1; i < userArray.size(); i++) {
            crossing.retainAll(userArray.get(i));
        }
        
        return crossing;
    }

    static HashSet<String> findUnion(ArrayList<ArrayList<String>> userArray) {
        HashSet<String> union = new HashSet<>(userArray.get(0));

        for (int j = 1; j < userArray.size(); j++) {
            union.addAll(userArray.get(j));
        }
        
        return union;
    }
}
