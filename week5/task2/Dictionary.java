package week5.task2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Dictionary {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Ввод: ");
            int n = Integer.valueOf(input.nextLine());
            Map<String, ArrayList<String>> userInput = new TreeMap<>(stringConverter(input, n));
            Map<String, ArrayList<String>> output = new TreeMap<>(mapHandler(userInput));
            System.out.println(output.keySet().size());
            output.forEach((i, j) -> System.out.println((i + " - " + j.toString().substring(1, j.toString().length() - 1))));
            
        }
    }

    static Map<String, ArrayList<String>> stringConverter(Scanner input, int q) {
        Map<String, ArrayList<String>> userInput = new HashMap<>();
            for (int i = 0; i < q; i++) {
                String[] englishLatinString = input.nextLine().split(",? ");
                String englishWord = englishLatinString[0];
                ArrayList<String> englishLatinArray = new ArrayList<>();
                for (int j = 1; j < englishLatinString.length; j++) {
                    englishLatinArray.add(englishLatinString[j]);
                    englishLatinArray.remove("-");
                }
                userInput.put(englishWord, new ArrayList<>(englishLatinArray));
            }

        return userInput;
    }

    static Map<String, ArrayList<String>> mapHandler(Map<String, ArrayList<String>> input) {
        Map<String, ArrayList<String>> outputMap = new HashMap<>();
        for (Map.Entry<String, ArrayList<String>> entry : input.entrySet()) {
            ArrayList<String> latinWords = new ArrayList<>(entry.getValue());
            for (int i = 0; i < latinWords.size(); i++) {
                ArrayList<String> words = new ArrayList<>();

                if(outputMap.containsKey(latinWords.get(i))){
                    words = outputMap.get(latinWords.get(i));
                    words.add(entry.getKey());
                } else {
                    words = new ArrayList<String>();
                    words.add(entry.getKey());
                    outputMap.put(latinWords.get(i), words);
                }
            }
        }

        return outputMap;
    }
}
