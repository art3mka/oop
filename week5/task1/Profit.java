package week5.task1;

import java.util.ArrayList;
import java.util.Scanner;

public class Profit {
    public static void main(String[] args) {
        try (Scanner userInput = new Scanner(System.in)) {
            System.out.print("Ввод: ");
            String stringWithDays = userInput.nextLine();
            ArrayList<Integer> days = WaterSquare.stringConverter(stringWithDays);
            System.out.print("Вывод: ");
            System.out.println(maxProfit(days));
        }
    }

    static int maxProfit(ArrayList<Integer> days) {
        int maxValue = 0;
        for (int i = 0; i < days.size() + 1; i++) {
            for (int j = i + 1; j < days.size(); j++) {
                int value = days.get(j) - days.get(i);
                if (value > maxValue) {
                    maxValue = value;
                }
            }
        }
    return maxValue;
    }
}