package week6;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import week6.support.Customer;
import week6.support.Order;
import week6.support.Product;

import java.time.Month;
import java.time.LocalDate;

public class PlacedOrders {
    public static void main(String[] args) {
        ArrayList<Order> ordersList = getOrder();
        double ordersSum = ordersSum(ordersList);
        System.out.println(ordersSum);
    }

    private static double ordersSum(ArrayList<Order> ordersList) {
        double ordersSum = ordersList.stream()
                .filter(order -> order.getOrderDate().getYear() == 2020
                && order.getOrderDate().getMonth().equals(Month.FEBRUARY))
                .flatMap(order -> order.getProducts().stream())
                .mapToDouble(Product::getPrice)
                .sum();
        return ordersSum;
    }

    private static ArrayList<Order> getOrder() {
        ArrayList<Order> ordersList = new ArrayList<>();
        Order firstOrder = new Order();
        Order secondOrder = new Order();
        Order thirdOrder = new Order();

        Customer сustomer = new Customer();
        сustomer.setId(1L);
        сustomer.setName("Jim Carrey");
        сustomer.setTier(1);

        Product vegetableProduct = new Product();
        vegetableProduct.setId(1L);
        vegetableProduct.setName("Carrot");
        vegetableProduct.setCategory("Vegetables");
        vegetableProduct.setPrice(40.5);

        Product computerPeripheralsProduct = new Product();
        computerPeripheralsProduct.setId(2L);
        computerPeripheralsProduct.setName("Keyboard");
        computerPeripheralsProduct.setCategory("Computer peripherals");
        computerPeripheralsProduct.setPrice(777.3);

        Product drinkProduct = new Product();
        drinkProduct.setId(3L);
        drinkProduct.setName("Cherry Juice");
        drinkProduct.setCategory("Drinks");
        drinkProduct.setPrice(85.2);

        Set<Product> products = new HashSet<Product>();
        products.add(vegetableProduct);
        products.add(computerPeripheralsProduct);
        products.add(drinkProduct);

        firstOrder.setId(1L);
        firstOrder.setOrderDate(LocalDate.of(2020, Month.FEBRUARY, 12));
        firstOrder.setDeliveryDate(LocalDate.of(2020, Month.FEBRUARY, 12));
        firstOrder.setStatus("Completed");
        firstOrder.setProducts(products);
        firstOrder.setCustomer(сustomer);

        secondOrder.setId(2L);
        secondOrder.setOrderDate(LocalDate.of(2020, Month.FEBRUARY, 14));
        secondOrder.setDeliveryDate(LocalDate.of(2020, Month.FEBRUARY, 15));
        secondOrder.setStatus("Completed");
        secondOrder.setProducts(products);
        secondOrder.setCustomer(сustomer);

        thirdOrder.setId(3L);
        thirdOrder.setOrderDate(LocalDate.of(2020, Month.MARCH, 26));
        thirdOrder.setDeliveryDate(LocalDate.of(2020, Month.MARCH, 26));
        thirdOrder.setStatus("Completed");
        thirdOrder.setProducts(products);
        thirdOrder.setCustomer(сustomer);

        ordersList.add(firstOrder);
        ordersList.add(secondOrder);
        ordersList.add(thirdOrder);

        return ordersList;
    }
}
