package week6;

import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SymbolFrequency {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Ввод: ");
            String userInput = input.nextLine();
            Map<Character, Long> frequency = 
                userInput.chars().mapToObj(symbol -> (char)symbol)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            
                Map.Entry<Character, Long> result = frequency.entrySet().stream()
                    .max(Map.Entry.comparingByValue()).get();

                System.out.printf("['%s', %d]", result.getKey(), result.getValue());
        }
    }
}

